import pygame

FPS = 60
ALWAYS_REPAINT = 2
REPAINT = 1
NO_REPAINT = 0
BALL_SIZE = 10


class Color:
    COLOR_YELLOW =   (250, 201, 64)
    COLOR_RED =      (233, 79, 69)
    COLOR_GREEN =    (168, 203, 77)
    COLOR_BLUE =     (75, 176, 158)
    COLOR_GREY =     (102, 102, 102)
    COLOR_DARKGREY = (80, 80, 80)
    COLOR_BG =       (28, 28, 28)
    KEY_A = pygame.K_a
    KEY_Z = pygame.K_z
    KEY_E = pygame.K_e
    KEY_R = pygame.K_r
