from math import cos, sin


def d2(v, w):
    return (v[0] - w[0]) ** 2 + (v[1] - w[1]) ** 2


def line_segment_distance(p, v, w):
    l2 = d2(v, w)
    if l2 == 0:
        return d2(p, v)
    t = ((p[0] - v[0]) * (w[0] - v[0]) + (p[1] - v[1]) * (w[1] - v[1])) / l2
    t = max(0, min(1, t))
    return d2(p, [v[0] + t * (w[0] - v[0]),
                  v[1] + t * (w[1] - v[1])])


def intersects(a1, a2, b1, b2):
    return line_segment_distance(b1, a1, a2) < 54


def rotate_point(px, py, cx, cy, a):
    # substract center
    px -= cx
    py -= cy
    # rotate
    px, py = px * cos(a) - py * sin(a), px * sin(a) + py * cos(a)
    # add center back
    px += cx
    py += cy
    return px, py
