import pygame

from config import ALWAYS_REPAINT, BALL_SIZE, Color, NO_REPAINT
from screen_size import window_width, window_height


class BallSprite(pygame.sprite.DirtySprite):
    def __init__(self, core, ball, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.core = core
        self.ball = ball
        self.rect = [(window_width - BALL_SIZE) // 2, (window_width - BALL_SIZE) // 2]
        self.image = pygame.Surface([BALL_SIZE, BALL_SIZE])
        self.image.fill(self.ball.color)
        self.dirty = ALWAYS_REPAINT

    def update(self, *args):
        self.image.fill(self.ball.color)
        self.rect = self.ball.current_pos


class Limit(pygame.sprite.DirtySprite):

    def __init__(self, core, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dirty = NO_REPAINT
        self.walls = core.walls
        self.image = pygame.Surface([window_width, window_height])
        self.image.set_colorkey((0, 0, 0))
        self.rect = [0, 0]
        pygame.draw.line(self.image, Color.COLOR_GREY, (self.walls.left_pos, self.walls.top_pos), (self.walls.right_pos, self.walls.top_pos))
        pygame.draw.line(self.image, Color.COLOR_GREY, (self.walls.left_pos, self.walls.bot_pos), (self.walls.right_pos, self.walls.bot_pos))
        pygame.draw.line(self.image, Color.COLOR_GREY, (self.walls.left_pos, self.walls.top_pos), (self.walls.left_pos, self.walls.bot_pos))
        pygame.draw.line(self.image, Color.COLOR_GREY, (self.walls.right_pos, self.walls.top_pos), (self.walls.right_pos, self.walls.bot_pos))


def get_bar_ui(color, x_pos, y_pos, binding_function, color_bg=Color.COLOR_GREY):

    class BarUI(pygame.sprite.DirtySprite):

        def __init__(self, core, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.dirty = ALWAYS_REPAINT
            self.core = core
            self.image = pygame.Surface([20,window_height*0.3])
            self.rect = [x_pos, y_pos]
            self.image.set_colorkey((0, 0, 0))

        def update(self):
            self.image.fill(color_bg)
            pygame.draw.rect(self.image, color, (0, (1 - max(0, binding_function())) * 0.3 * window_height, 20, window_height * 0.3))

    return BarUI


class ButtonSprite(pygame.sprite.DirtySprite):

    def __init__(self, core, x_pos, y_pos, ratio_x, ratio_y, color_text, color_bgd, color_click, label, font_size, function_on_click, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.core = core
        self.dirty = NO_REPAINT
        self.image = pygame.Surface([window_width*ratio_x, window_height*ratio_y])
        self.rect = [x_pos-window_width*(ratio_x/2), y_pos-window_height*(ratio_y/2)]
        self.color_text = color_text
        self.color_bgd = color_bgd
        self.color_click = color_click
        self.label = label
        self.function_on_click = function_on_click
        self.font_size = font_size
        self.update_ui(color_bgd)

    def button_on_focus(self):
        bt_x, bt_y = self.core.ball.current_pos
        return ((bt_x >= (self.rect[0]) and bt_x <= (self.rect[0] + self.image.get_width())) and
            (bt_y >= (self.rect[1]) and bt_y <= (self.rect[1] + self.image.get_height())))

    def on_click(self):
        self.function_on_click()

    def update_ui(self,color):
        pygame.font.init()
        self.image.fill(color)
        self.font = pygame.font.Font("assets/fonts/Cyberspace.otf", self.font_size)
        image_surface = self.font.render(self.label, True, self.color_text)
        dest = ((self.image.get_width()-image_surface.get_width())/2, (self.image.get_height()-image_surface.get_height())/2)
        self.image.blit(image_surface, dest)


class TitleLabel(pygame.sprite.DirtySprite):

    def __init__(self, core, x_pos, y_pos, color_text, label, color_bgd, font_size, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.core = core
        self.dirty = NO_REPAINT
        self.color_text = color_text
        self.color_bgd = color_bgd
        self.font_size = font_size
        self.label = label
        self.image = pygame.Surface([window_width*0.4, window_height*0.10])
        self.rect = [x_pos-window_width*0.2, y_pos-window_height*0.025]
        self.create_ui()

    def create_ui(self):
        pygame.font.init()
        # BackgroundColor -> default = None
        if self.color_bgd is not None:
            self.image.fill(self.color_bgd)
        self.font = pygame.font.Font("assets/fonts/Star-Strella.ttf", self.font_size)
        image_surface = self.font.render(self.label, True, self.color_text)
        dest = ((self.image.get_width()-image_surface.get_width())/2, (self.image.get_height()-image_surface.get_height())/2)
        self.image.blit(image_surface, dest)

    def update_label(self, label_changed):
        self.label = label_changed
        self.create_ui()


class FontLabel(pygame.sprite.DirtySprite):

    def __init__(self, core, x_pos, y_pos, ratio_x, ratio_y, color_text, label, color_bgd, font_size, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.core = core
        self.dirty = NO_REPAINT
        self.color_text = color_text
        self.color_bgd = color_bgd
        self.font_size = font_size
        self.label = label
        self.image = pygame.Surface([window_width*ratio_x, window_height*ratio_y])
        self.rect = [x_pos-window_width*(ratio_x/2), y_pos-window_height*(ratio_y/2)]
        self.create_ui()

    def create_ui(self):
        pygame.font.init()
        if self.color_bgd is not None:
            self.image.fill(self.color_bgd)
        self.font = pygame.font.Font("assets/fonts/Cyberspace.otf", self.font_size)
        image_surface = self.font.render(self.label, True, self.color_text)
        dest = ((self.image.get_width()-image_surface.get_width())/2, (self.image.get_height()-image_surface.get_height())/2)
        self.image.blit(image_surface, dest)

    def update_label(self, label_changed):
        self.label = label_changed
        self.create_ui()
