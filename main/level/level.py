import random
from obstacle.model import PolygonObstacle, StarObstacle, HealthBonus, PointBonus, WinBonus, PotatObstacle, CrossObstacle
import json
from screen_size import window_width, window_height
from config import Color

colors = {
    "red": Color.COLOR_RED,
    "yellow": Color.COLOR_YELLOW,
    "green": Color.COLOR_GREEN,
    "blue": Color.COLOR_BLUE,
}


class Level:

    def __init__(self, core):
        self.core = core
        self._objects = self._object_generator()

    def _object_generator(self):
        pass

    def next(self):
        return self._objects.__next__()


class RandomPolygonsFallingFromTop(Level):

    def __init__(self, core):
        super().__init__(core)

    def _object_generator(self):
        while True:
            ans = []
            if random.random() < 0.01:
                n = random.randint(3, 7) * 2
                diam = random.randint(60, 240)
                ans.append(PotatObstacle(n, diam))
                x_pos = random.randint(int(window_width * 0.2), int(window_width * 0.8))
                r_angle = random.uniform(-0.06, 0.06)
                speed = random.uniform(0.5, 2)
                ans[-1].translate(x_pos, 0)
                ans[-1].default_rotation = lambda x, r_angle=r_angle: x.rotate(r_angle)
                ans[-1].default_translation = lambda x, speed=speed: x.translate(0, speed)
            if random.random() < 0.001:
                diam = random.randint(10, 24)
                x_pos = random.randint(int(window_width * 0.2), int(window_width * 0.8))
                y_pos = random.randint(int(window_height * 0.2), int(window_height * 0.8))
                ans.append(PointBonus(x_pos, y_pos, diam))
            yield ans


class FileLevel(Level):

    def __init__(self, core, filename):
        self.dict = json.loads(open(filename).read())
        super().__init__(core)
        if "character" in self.dict:
            for k, v in self.dict["character"].items():
                self.core.character.__setattr__(k, v)

    def _object_generator(self):
        obstacles = {
            "potato": PotatObstacle,
            "star": StarObstacle,
            "obstacle": PolygonObstacle,
            "cross": CrossObstacle,
        }
        bonus = {
            "hp": HealthBonus,
            "pts": PointBonus,
            "win": WinBonus,
        }
        n = 0
        while True:
            n += 1
            ans = []
            if str(n) in self.dict:
                for i in range(len(self.dict[str(n)])):
                    t = self.dict[str(n)][i].get("type", "obstacle")
                    sides = self.dict[str(n)][i].get("n", 4)
                    diam = self.dict[str(n)][i].get("diam", 100)
                    x_pos = self.dict[str(n)][i].get("x_pos", 0.5) * window_width
                    y_pos = self.dict[str(n)][i].get("y_pos", 0.5) * window_height
                    r_angle = self.dict[str(n)][i].get("r_angle", 0)
                    angle = self.dict[str(n)][i].get("angle", 0)
                    x_speed = self.dict[str(n)][i].get("x_speed", 0)
                    y_speed = self.dict[str(n)][i].get("y_speed", 0)
                    lifespan = self.dict[str(n)][i].get("lifespan", None)
                    color = [colors[x] for x in self.dict[str(n)][i].get("color", ["blue"])]
                    if t in obstacles:
                        ans.append(obstacles[t](sides, diam, color=color))
                        ans[-1].translate(x_pos, y_pos)
                        ans[-1].rotate(angle)
                        ans[-1].default_rotation = lambda x, r_angle=r_angle: x.rotate(r_angle)
                        ans[-1].default_translation = lambda x, x_speed=x_speed, y_speed=y_speed: x.translate(x_speed, y_speed)
                    elif t in bonus:
                        ans.append(bonus[t](x_pos, y_pos, diam))
                        ans[-1].default_translation = lambda x, x_speed=x_speed, y_speed=y_speed: x.translate(x_speed, y_speed)
                    elif t == "timeout":
                        self.dict[str(n)][i]["target"].destroy()
                    if lifespan is not None:
                        if str(n + lifespan) not in self.dict:
                            self.dict[str(n + lifespan)] = []
                        self.dict[str(n + lifespan)].append({"type": "timeout", "target": ans[-1]})
            yield ans


