from config import Color


class Obstacle:

    def __init__(self, *args, **kwargs):
        self.fragments = []

    def collide(self, ball):
        return [x for x in self.fragments if x.collide(ball)]

    def create_ui(self, core, *groups):
        return [f.create_ui(core, *groups) for f in self.fragments]

    def is_in_screen(self):
        raise NotImplemented


class Disableable(Obstacle):

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.is_enabled = True

    def disable(self):
        self.is_enabled = False
        for f in self.fragments:
            f.color = Color.COLOR_GREY

class Collidable(Disableable):

    def collides(self, character):
        character.remove_health(10)
        self.disable()


class Translatable(Obstacle):

    def translate(self, dx, dy):
        for frag in self.fragments:
            frag.translate(dx, dy)


class DefaultTranslation(Translatable):

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.default_translation = lambda x: None

    def move(self):
        self.default_translation(self)


class Rotatable(Obstacle):

    def __init__(self, center, *args, **kwargs):
        super().__init__()
        self.center = list(center)

    def rotate(self, a):
        for frag in self.fragments:
            frag.rotate(a, self.center)

    @property
    def cx(self):
        return self.center[0]

    @cx.setter
    def cx(self, value):
        self.center[0] = value

    @property
    def cy(self):
        return self.center[1]

    @cy.setter
    def cy(self, value):
        self.center[1] = value


class DefaultRotation(Rotatable):

    def __init__(self, center, *args, **kwargs):
        super().__init__(center)
        self.default_rotation = lambda x: None

    def move(self):
        self.default_rotation(self)


class DefaultMovements(DefaultRotation, DefaultTranslation):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def move(self):
        for cls in DefaultMovements.__bases__:
            cls.move(self)

    def translate(self, dx, dy):
        super().translate(dx, dy)
        self.cx += dx
        self.cy += dy


class Consumable(Collidable):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.alive = True
        self.is_enabled = False

    def is_in_screen(self):
        return self.alive

    def collides(self, character):
        self.destroy()

    def destroy(self):
        self.alive = False
