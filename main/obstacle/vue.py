import pygame


class LineObsFragmentUI:

    def __init__(self, core, line, *args, **kwargs):
        self.line = line
        self.image = core.screen

    def update(self):
        pygame.draw.line(self.image, self.line.color, self.line.p1, self.line.p2, 10)


class DiscObsFragmentUI:
    def __init__(self, core, disc, *args, **kwargs):
        self.disc = disc
        self.image = core.screen

    def update(self):
        pygame.draw.circle(self.image, self.disc.color, (int(self.disc.p1x), int(self.disc.p1y)), self.disc.diam)
