import random

from config import Color
from helpers import intersects, rotate_point
from math import pi
from screen_size import window_width, window_height

from obstacle.mixins import DefaultMovements, Collidable
from obstacle.fragments import DiscObstacle, LineObstacle
from obstacle.mixins import DefaultTranslation, Consumable


class PolygonObstacle(DefaultMovements, Collidable):

    def __init__(self, n, diameter, color=None, precision=0):
        if color is None:
            color = [Color.COLOR_BLUE, Color.COLOR_RED, Color.COLOR_YELLOW, Color.COLOR_GREEN]
        assert n > 1
        assert 0 <= precision < 1
        self.diameter = diameter
        super().__init__((0, 0))
        p = [(0, diameter)]
        for _ in range(n-1):
            a = 2 * pi / n
            if precision !=0: a *= random.uniform(1-precision, 1+precision)
            p.append((rotate_point(*p[-1], 0, 0, a)))

        for i in range(n):
            self.fragments.append(LineObstacle(color[i % len(color)], p[i], p[i-1]))

    def is_in_screen(self):
        return (-self.diameter) < self.cx < window_width + self.diameter \
               and (-self.diameter) < self.cy < window_height + self.diameter


class HealthBonus(DefaultTranslation, Consumable):
    def __init__(self, x_pos, y_pos, diam, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fragments.append(DiscObstacle(Color.COLOR_RED, (x_pos, y_pos), diam))

    def collides(self, character):
        character.heal(15)
        super().collides(character)


class PointBonus(DefaultTranslation, Consumable):
    def __init__(self, x_pos, y_pos, diam, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fragments.append(DiscObstacle(Color.COLOR_BLUE, (x_pos, y_pos), diam))

    def collides(self, character):
        character.score += 10
        super().collides(character)


class WinBonus(DefaultTranslation, Consumable):
    def __init__(self, x_pos, y_pos, diam, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fragments.append(DiscObstacle(Color.COLOR_GREEN, (x_pos, y_pos), diam))

    def collides(self, character):
        character.won = True
        super().collides(character)

class PotatObstacle(DefaultMovements, Collidable):

    def __init__(self, n, diameter, ratio=0.6, color=None):
        if color is None:
            color = [Color.COLOR_BLUE, Color.COLOR_RED, Color.COLOR_YELLOW, Color.COLOR_GREEN]
        assert n > 3
        assert n %2 == 0
        self.diameter = diameter
        self.inner_diameter = self.diameter * ratio
        super().__init__((0, 0))
        a = 2 * pi / n
        p = [(0, self.diameter), rotate_point(0, self.inner_diameter, 0, 0, a)]
        for _ in range(n-2):
            p.append((rotate_point(*p[-2], 0, 0, a)))

        for i in range(n):
            self.fragments.append(LineObstacle(color[i % len(color)], p[i], p[i-1]))

    def is_in_screen(self):
        return (-self.diameter) < self.cx < window_width + self.diameter \
               and (-self.diameter) < self.cy < window_height + self.diameter


class StarObstacle(DefaultMovements, Collidable):

    def __init__(self, n, diameter, ratio=0.6, color=None):
        if color is None:
            color = [Color.COLOR_BLUE, Color.COLOR_RED, Color.COLOR_YELLOW, Color.COLOR_GREEN]
        assert n > 3
        assert n % 2 == 0
        self.diameter = diameter
        self.inner_diameter = self.diameter * ratio
        super().__init__((0, 0))
        a = 2 * pi / n
        p = [(0, self.diameter), rotate_point(0, self.inner_diameter, 0, 0, a)]
        for _ in range(n-2):
            p.append((rotate_point(*p[-2], 0, 0, 2 * a)))

        for i in range(n):
            self.fragments.append(LineObstacle(color[i % len(color)], p[i], p[i-1]))

    def is_in_screen(self):
        return (-self.diameter) < self.cx < window_width + self.diameter \
               and (-self.diameter) < self.cy < window_height + self.diameter


class CrossObstacle(DefaultMovements, Collidable):

    def __init__(self, n, diameter, color=None, precision=0):
        if color is None:
            color = [Color.COLOR_BLUE, Color.COLOR_RED, Color.COLOR_YELLOW, Color.COLOR_GREEN]
        assert n > 1
        assert 0 <= precision < 1
        self.diameter = diameter
        super().__init__((0, 0))
        p = [(0, diameter)]
        for _ in range(n-1):
            a = 2 * pi / n
            if precision !=0: a *= random.uniform(1-precision, 1+precision)
            p.append((rotate_point(*p[-1], 0, 0, a)))

        for i in range(n):
            self.fragments.append(LineObstacle(color[i % len(color)], p[i], self.center))

    def is_in_screen(self):
        return (-self.diameter) < self.cx < window_width + self.diameter \
               and (-self.diameter) < self.cy < window_height + self.diameter