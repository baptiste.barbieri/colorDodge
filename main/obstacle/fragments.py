from config import Color, BALL_SIZE
from helpers import intersects, rotate_point, d2
from obstacle.vue import LineObsFragmentUI,DiscObsFragmentUI


class ObsFragment:

    def __init__(self, color):
        self.color = color

    def collide(self, ball):
        return ball.color != self.color and self.color != Color.COLOR_GREY


class LineObstacle(ObsFragment):

    def __init__(self, color, p1, p2):
        super().__init__(color)
        self.p1 = list(p1)
        self.p2 = list(p2)

    def collide(self, ball):
        return super().collide(ball) and intersects(self.p1, self.p2, ball.current_pos, ball.last_pos)

    @property
    def p1x(self):
        return self.p1[0]

    @property
    def p2x(self):
        return self.p2[0]

    @property
    def p1y(self):
        return self.p1[1]

    @property
    def p2y(self):
        return self.p2[1]

    @p1x.setter
    def p1x(self, value):
        self.p1[0] = value

    @p2x.setter
    def p2x(self, value):
        self.p2[0] = value

    @p1y.setter
    def p1y(self, value):
        self.p1[1] = value

    @p2y.setter
    def p2y(self, value):
        self.p2[1] = value

    def translate(self, dx, dy):
        self.p1x += dx
        self.p2x += dx
        self.p1y += dy
        self.p2y += dy

    def rotate(self, a, center):
        self.p1x, self.p1y = rotate_point(self.p1x, self.p1y, *center, a)
        self.p2x, self.p2y = rotate_point(self.p2x, self.p2y, *center, a)

    def create_ui(self, core, *groups):
        return LineObsFragmentUI(core, self, *groups)


class DiscObstacle(ObsFragment):

    def __init__(self, color, p1, diam):
        super().__init__(color)
        self.p1 = list(p1)
        self.diam = diam

    def collide(self, ball):
        return d2(self.p1, ball.current_pos) < (self.diam + BALL_SIZE + 1) ** 2

    @property
    def p1x(self):
        return self.p1[0]

    @property
    def p1y(self):
        return self.p1[1]

    @p1x.setter
    def p1x(self, value):
        self.p1[0] = value

    @p1y.setter
    def p1y(self, value):
        self.p1[1] = value

    def translate(self, dx, dy):
        self.p1x += dx
        self.p1y += dy

    def rotate(self, a, center):
        self.p1x, self.p1y = rotate_point(self.p1x, self.p1y, self.p1x, self.p1y, a)

    def create_ui(self, core, *groups):
        return DiscObsFragmentUI(core, self, *groups)
