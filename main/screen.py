import pygame
import os

from model import Ball, Walls, Character
from config import Color, FPS
from vue import BallSprite, Limit, get_bar_ui

os.environ['SDL_VIDEO_CENTERED'] = '1'


class Core:
    def __init__(self, surface, name):
        pygame.display.set_caption(name)
        self.screen = surface
        self.clock = pygame.time.Clock()
        self.event_manager = EventManager(self)
        self.all_sprites = pygame.sprite.LayeredUpdates()
        self.walls = Walls(self)
        self.ball = Ball(self.walls)
        Limit(self, self.all_sprites)

        pygame.mouse.set_visible(False)
        self.init()
        self.post_init()

    def dispatch(self, event):
        self.event_manager.dispatch(event)

    def run(self):
        keep_going = True
        while keep_going:
            self.pre_loop()
            keep_going = self.main_loop()
            self.post_loop()

    def post_loop(self):
        pygame.display.update()

    def main_loop(self):
        for event in pygame.event.get():
            self.dispatch(event)

        self.ball.update_pos()
        self.all_sprites.update()
        self.all_sprites.draw(self.screen)
        return True

    def pre_loop(self):
        self.clock.tick(FPS)
        self.screen.fill(Color.COLOR_BG)

    def post_init(self):
        self.ball_sprite = BallSprite(self, self.ball, self.all_sprites)

    def init(self):
        pass


class EventManager:

    def __init__(self, core, *event_handlers):
        self.core = core
        self.event_handlers = list(*event_handlers)

    def dispatch(self, event):
        for event_handler in self.event_handlers:
            if event_handler(self.core, event):
                return

    def register_event_handler(self, event_handler):
        self.event_handlers.append(event_handler)

    def event_handler(self, f):
        self.register_event_handler(f)
        return f

