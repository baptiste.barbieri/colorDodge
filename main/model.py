from math import sqrt

import pygame
from screen_size import window_width, window_height
from config import Color, BALL_SIZE

MAX_SPEED = 48


class Ball:
    def __init__(self, walls):
        self.color = Color.COLOR_YELLOW
        self.walls = walls
        self.current_pos = pygame.mouse.get_pos()
        self.last_pos = self.current_pos

    def update_pos(self):  # must be called at every game loop
        dx, dy = pygame.mouse.get_rel()
        max_r_speed = MAX_SPEED / (1+sqrt(min(max(dx, -dx), max(dy, -dy)/max(max(dx, -dx), max(dy, -dy), 1))))
        dx = max(-max_r_speed, min(max_r_speed, dx))
        dy = max(-max_r_speed, min(max_r_speed, dy))
        pos_x = self.current_pos[0] + dx
        pos_y = self.current_pos[1] + dy

        if pos_x > self.walls.right_pos - BALL_SIZE:
            pos_x = self.walls.right_pos - BALL_SIZE
        elif pos_x < self.walls.left_pos:
            pos_x = self.walls.left_pos

        if pos_y < self.walls.top_pos:
            pos_y = self.walls.top_pos
        elif pos_y > self.walls.bot_pos - BALL_SIZE:
            pos_y = self.walls.bot_pos - BALL_SIZE
        self.last_pos = self.current_pos
        self.current_pos = pos_x, pos_y


class Walls:

    def __init__(self, core):
        self.left_pos = 0.1 * window_width
        self.top_pos = 0.1 * window_height
        self.bot_pos = 0.9 * window_height
        self.right_pos = 0.9 * window_width


class Character:

    def __init__(self, core):
        self.core = core
        self.ball = core.ball
        self.health = 100
        self.health_max = 100
        self.health_regen = 0
        self.stamina = 100
        self.stamina_max = 100
        self.stamina_regen = 0.3
        self.stamina_cost = 20
        self.immunity_time = 600
        self.last_immune = None
        self.score = 0
        self.won = False

    def get_percent_health(self):
        return self.health / self.health_max

    def get_percent_stamina(self):
        return self.stamina / self.stamina_max

    def remove_health(self, h):
        if self.last_immune is None or self.last_immune + self.immunity_time < pygame.time.get_ticks():
            self.health -= h
            self.last_immune = pygame.time.get_ticks()

    def heal(self, h):
        self.health = min(self.health_max, self.health + h)
    
    def regen(self):
        self.health = min(self.health + self.health_regen, self.health_max)
        self.stamina = min(self.stamina + self.stamina_regen, self.stamina_max)

    def change_color(self, color):
        if color != self.ball.color and self.stamina >= self.stamina_cost:
            self.stamina -= self.stamina_cost
            self.ball.color = color

