import pygame
import sys

from config import Color


def quit_event(core, event):
    if event.type == pygame.QUIT:
        sys.exit()
    elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
        sys.exit()
    return False


def change_color_event(core, event):
    if event.type == pygame.KEYDOWN and event.unicode == "a":
        core.character.change_color(Color.COLOR_YELLOW)
        return True
    if event.type == pygame.KEYDOWN and event.unicode == "z":
        core.character.change_color(Color.COLOR_RED)
        return True
    if event.type == pygame.KEYDOWN and event.unicode == "e":
        core.character.change_color(Color.COLOR_GREEN)
        return True
    if event.type == pygame.KEYDOWN and event.unicode == "r":
        core.character.change_color(Color.COLOR_BLUE)
        return True
    return False


def key_binding_quit(core, event):
    if event.type == pygame.KEYDOWN and event.unicode == "m":
        core.character.health = float("-inf")
        return True
    return False


def on_mouseover_button(core, event):
    if pygame.mouse.get_focused():
        for obj in core.list_btn:
            if obj.button_on_focus():
                obj.update_ui(Color.COLOR_DARKGREY)
            else:
                obj.update_ui(Color.COLOR_GREY)


def on_click(core, event):
    if pygame.mouse.get_focused():
        if event.type == pygame.MOUSEBUTTONUP:
            for obj in core.list_btn:
                if obj.button_on_focus():
                    obj.on_click()
