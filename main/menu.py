import sys

from config import Color, FPS
from vue import BallSprite, Limit, ButtonSprite, TitleLabel, FontLabel
from events import quit_event, change_color_event, on_mouseover_button, on_click, key_binding_quit

from core import GameCore
from screen import Core
from screen_size import screen, window_width, window_height


class Menu(Core):

    def __init__(self, surface, name):
        super().__init__(surface, name)
        # assigning self.character.change_color to bypass energy use in menu
        self.character = type('', (object,), {"change_color": lambda x, self=self: self.ball.__setattr__("color", x)})

    def init(self):
        self.title = TitleLabel(self, window_width/2, window_height/3, Color.COLOR_GREY, "ColorDodge", Color.COLOR_BG, min(72,int(window_width/100*4.1)), self.all_sprites)

        self.dic_label_to_json = {
            "Level selected : 1" : "levels/level1.json",
            "Level selected : 2" : "levels/level2.json",
            "Level selected : 3" : "levels/level3.json"
        }

        self.lables_d = {
            "Level selected : 1" : "Level selected : 2",
            "Level selected : 2" : "Level selected : 3",
            "Level selected : 3" : "Level selected : 1"
        }

        self.btn_play_regular = ButtonSprite(self, window_width/2, window_height/3+15+window_height*0.10, 0.2, 0.05, Color.COLOR_BG, Color.COLOR_GREY, Color.COLOR_GREEN, "Play Level", min(26,int(window_width/100*1.5)), lambda: self.start_game(self.dic_label_to_json[self.label_levels.label]), self.all_sprites)

        self.label_levels = FontLabel(self, window_width*0.475, window_height/3+30+window_height*0.10+window_height*0.05*1, 0.15, 0.05, Color.COLOR_GREY, "Level selected : 1", Color.COLOR_BG, min(18,int(window_width/100*1)), self.all_sprites)
        self.btn_change_level = ButtonSprite(self, window_width*0.575, window_height/3+30+window_height*0.10+window_height*0.05*1, 0.05, 0.05, Color.COLOR_BG, Color.COLOR_GREY, Color.COLOR_GREEN, "<>", min(26,int(window_width/100*1.5)), self.change_level, self.all_sprites)

        self.btn_play_infinity = ButtonSprite(self, window_width/2, window_height/3+45+window_height*0.10+window_height*0.05*2, 0.2, 0.05, Color.COLOR_BG, Color.COLOR_GREY, Color.COLOR_GREEN, "Play Inf", min(26,int(window_width/100*1.5)), lambda: self.start_game(None), self.all_sprites)

        self.btn_quit = ButtonSprite(self, window_width/2, window_height/3+window_height*0.10+60+window_height*0.05*3, 0.2, 0.05, Color.COLOR_BG, Color.COLOR_GREY, Color.COLOR_GREEN, "Quit", min(26,int(window_width/100*1.5)), sys.exit, self.all_sprites)

        self.label_score = FontLabel(self, window_width/2, window_height/3+75+window_height*0.10*4, 0.2, 0.05, Color.COLOR_GREY, "", Color.COLOR_BG, min(26,int(window_width/100*1.5)), self.all_sprites)

        self.list_btn = [self.btn_play_regular, self.btn_play_infinity, self.btn_quit, self.btn_change_level]

    def change_level(self):
        self.label_levels.update_label(self.lables_d[self.label_levels.label])

    def start_game(self, levelfile):
        main_game = GameCore(screen, 'ColorDodge', levelfile)
        main_game.event_manager.event_handler(quit_event)
        main_game.event_manager.event_handler(change_color_event)
        main_game.event_manager.event_handler(key_binding_quit)
        won, score = main_game.run()
        self.label_score.update_label("Score : %d" % score)
        self.title.update_label("Victory" if won else "Game Over")


main_menu = Menu(screen, 'ColorDodge')
main_menu.event_manager.event_handler(quit_event)
main_menu.event_manager.event_handler(change_color_event)
main_menu.event_manager.event_handler(on_mouseover_button)
main_menu.event_manager.event_handler(on_click)


if __name__ == '__main__':
    main_menu.run()
