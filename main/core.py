import itertools

from config import Color
from level.level import RandomPolygonsFallingFromTop, FileLevel
from model import Character
from vue import get_bar_ui, FontLabel

from screen_size import window_width, window_height
from screen import Core


class GameCore(Core):
    def __init__(self, surface, name, levelfile=None):
        super().__init__(surface, name)
        self.collision_manager = CollisionManager(self, self.ball)
        self.character = Character(self)
        self.bar_health = get_bar_ui(Color.COLOR_RED, window_width - 35, window_height * 0.7 - 15, self.character.get_percent_health)(self, self.all_sprites)
        self.bar_stamina = get_bar_ui(Color.COLOR_YELLOW, window_width - 70, window_height * 0.7 - 15, self.character.get_percent_stamina)(self, self.all_sprites)

        self.label_a = FontLabel(self, window_width*0.975, window_height/10+15, 0.05, 0.05, Color.COLOR_BG, "A", Color.COLOR_YELLOW, min(26,int(window_width/100*1.2)), self.all_sprites)
        self.label_z = FontLabel(self, window_width*0.975, window_height/10+30+ window_height*0.05, 0.05, 0.05, Color.COLOR_BG, "Z", Color.COLOR_RED, min(26,int(window_width/100*1.2)), self.all_sprites)
        self.label_e = FontLabel(self, window_width*0.975, window_height/10+45+ window_height*0.05*2, 0.05, 0.05, Color.COLOR_BG, "E", Color.COLOR_GREEN, min(26,int(window_width/100*1.2)), self.all_sprites)
        self.label_r = FontLabel(self, window_width*0.975, window_height/10+60+ window_height*0.05*3, 0.05, 0.05, Color.COLOR_BG, "R", Color.COLOR_BLUE, min(26,int(window_width/100*1.2)), self.all_sprites)

        self.label_quit = FontLabel(self, window_width*0.050, window_height-window_height*0.05, 0.1, 0.05, Color.COLOR_BG, "M : Menu", Color.COLOR_GREY, min(26,int(window_width/100*1.2)), self.all_sprites)

        self.label_score = FontLabel(self, window_width*0.075, window_height*0.05, 0.15, 0.05, Color.COLOR_BG, "Score : 0", Color.COLOR_GREY, min(26,int(window_width/100*1.2)), self.all_sprites)

        self.level = FileLevel(self, levelfile) if levelfile is not None else RandomPolygonsFallingFromTop(self)
        self._obstacles = {}

    @property
    def obstacles(self):
        return self._obstacles.keys()

    @property
    def obstacle_views(self):
        return self._obstacles.values()

    def add_obstacle(self, o):
        self._obstacles[o] = o.create_ui(self)

    def remove_out_of_screen_obstacles(self):
        o = {}
        for k, v in self._obstacles.items():
            if k.is_in_screen():
                o[k] = v
            elif k.is_enabled:
                self.character.score += 1
        self._obstacles = o

    def lost_game(self):
        self.keep_going = False

    def main_loop(self):
        super().main_loop()
        # adding obstacle from level generator
        next_objects = self.level.next()
        for next_object in next_objects:
            if next_object is not None:
                self.add_obstacle(next_object)

        for o in self.obstacles:
            o.move()

        self.remove_out_of_screen_obstacles()

        self.ball.update_pos()
        for x in itertools.chain(*self.obstacle_views):
            x.update()

        a = self.collision_manager.collides()
        for collided in a:
            collided.collides(self.character)

        self.character.regen()
        self.label_score.update_label("Score : %d" % self.character.score)
        return not self.character.won and self.character.health > 0

    def run(self):
        super().run()
        return self.character.won, self.character.score


class CollisionManager:

    def __init__(self, core, ball):
        self.ball = ball
        self.core = core

    def add(self, obj):
        self.core.obstacles.append(obj)

    def collides(self):
        ans = []
        for obj in self.core.obstacles:
            if obj.collide(self.ball):
                ans.append(obj)
        return ans
