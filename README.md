# Color Dodge

Projet universitaire au sein de la licence 3 d'Orléans, spécialité ingénierie informatique de l'année 2018.

## Membres

* BARBIERI Baptiste,
* DERIEUX Arnault,
* ROBIN Anthony.

## Utilisation

```
python3 ./core.py
```

### Dernière mise à jour

22/01/2018
